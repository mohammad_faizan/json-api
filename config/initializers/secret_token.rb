# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
JsonApi::Application.config.secret_key_base = 'bd2dd45537c5ae99de950f93ab5ed682f2a239180bf15c72ad967612a72ee0e6da842ec28870a8906dbd07f9a8d83681bd2e38acbbba79b49e11e3adf5562b32'
